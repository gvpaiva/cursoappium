package Testes;

import Pages.MenuPage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OpcaoEscondidaTeste extends BaseTest {
    private MenuPage menu = new MenuPage();
    @Test
    public void deveEncontrarOpcaoEscondida() {
        System.out.println(menu.existeOpcao());
        //Rolar
        menu.scrollDown();
        //Clicar Menu
        menu.clicarPorTexto("Opção bem escondida");
        //Verificar Mensagem
        Assert.assertEquals(menu.obterMensagemAlerta(), "Você achou essa opção");
        //Sair
        menu.clicarPorTexto("OK");

    }
}
