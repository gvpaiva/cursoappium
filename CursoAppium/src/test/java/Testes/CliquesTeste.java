package Testes;

import Pages.CliquesPage;
import Pages.MenuPage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CliquesTeste extends BaseTest {
    private MenuPage menu = new MenuPage();
    private CliquesPage cliques = new CliquesPage();
    @BeforeTest
    public void setUp() {
        //acessar menu
        menu.acessarCliques();
    }
    @Test
    public void deveFazerUmCliqueLongo() {
        //clicar longamente em clique longo
        cliques.cliqueLongo();

        //verificar texto
        Assert.assertEquals(cliques.obterTextoCampo(), "Clique Longo");
    }
    @Test
    public void deveFazerUmCliqueDuplo() {
        //clicar longamente em clique longo
        cliques.clicarPorTexto("Clique duplo");
        cliques.clicarPorTexto("Clique duplo");

        //verificar texto
        Assert.assertEquals(cliques.obterTextoCampo(), "Duplo Clique");
    }

}
