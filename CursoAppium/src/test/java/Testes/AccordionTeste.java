package Testes;

import Pages.AccordionPage;
import Pages.MenuPage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AccordionTeste extends BaseTest {
    private MenuPage menu = new MenuPage();
    private AccordionPage accordion = new AccordionPage();
    @Test
    public void deveInteragirComAccordion(){
        //Acessar menu
        menu.acessarAccordion();
        //Clicar op1
        accordion.clicarOp1();
        //verificar texto op1
        Assert.assertTrue(accordion.obterTextoOp1(), "Esta é a descrição da opção 1");
    }
}
