package Testes;

import Pages.AbaPage;
import Pages.MenuPage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AbaTeste extends BaseTest {
    private MenuPage menu = new MenuPage();
    private AbaPage aba = new AbaPage();
    @Test
    public void navegarPelasAbas (){
        //Entrar em Abas
        menu.acessarAbas();
        //Obter texto da aba 1
        Assert.assertTrue(aba.obterTextoAba1(), "Este é o conteúdo da Aba 1");

        //clicar em aba 2
        aba.selecionarAba2();
        //obter texto aba 2
        Assert.assertTrue(aba.obterTextoAba2(), "Este é o conteúdo da Aba 2");
    }
}
