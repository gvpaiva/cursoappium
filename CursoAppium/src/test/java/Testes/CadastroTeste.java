package Testes;

import Pages.FormularioPage;
import Pages.MenuPage;

import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CadastroTeste extends BaseTest {
    private MenuPage menu = new MenuPage();
    private FormularioPage formulario = new FormularioPage();

    @Test
    public void deveFazerCadastro() {
        menu.acessarFormulario();

        formulario.escreverNome("Gustavo Varela Paiva");
        Assert.assertEquals(formulario.obterNome(), "Gustavo Varela Paiva");

        formulario.selecionarCombo("PS4");
        String textoConsole = formulario.obterTexo();
        Assert.assertEquals(textoConsole, "PS4");

        Assert.assertFalse(formulario.isCheckMarcado());
        Assert.assertTrue(formulario.isSwitchMarcado());

        formulario.clicarCheck();
        formulario.clicarSwitch();

        Assert.assertTrue(formulario.isCheckMarcado());
        Assert.assertFalse(formulario.isSwitchMarcado());

        formulario.clicarSalvar();

        String textoNomeCadastro = formulario.obterNomeCadastrado();
        Assert.assertEquals(textoNomeCadastro, "Nome: Gustavo Varela Paiva");

        String textoConsoleCadastro = formulario.obterConsoleCadastrado();
        Assert.assertEquals(textoConsoleCadastro, "Console: ps4");
    }
}

