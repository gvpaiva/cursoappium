package Testes;

import Pages.AlertaPage;
import Pages.MenuPage;
import core.BaseTest;
import org.aspectj.lang.annotation.Before;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class AlertTeste extends BaseTest {
    private MenuPage menu = new MenuPage();
    private AlertaPage alerta = new AlertaPage();

    @BeforeTest
    public void setUp() {
        //acessar menu alerta
        menu.acessarAlerta();
    }

    @Test
    public void deveConfirmarAlerta() {
        //clicar em alertar confirm
        alerta.clicarAlertaConfirm();

        //verificar os textos
        Assert.assertEquals(alerta.obterTituloAlerta(), "Info");
        Assert.assertEquals(alerta.obterMensagemAlerta(), "Confirma a operação?");

        //confirmar alerta
        alerta.clicarConfirmar();

        //verificar nova mensagem
        alerta.esperarMensagemFicarVisivel();
        Assert.assertEquals(alerta.obterMensagemAlerta(), "Confirmado");

        //sair
        alerta.clicarSair();

    }

    @Test
    public void deveClicarForaAlerta() {
        alerta.clicarAlertaSimples();
        alerta.esperarConfirmarFicarVisivel();
        alerta.clicarForaCaixa();

        Assert.assertFalse(alerta.existeElementoPorTexo("Pode clicar no OK ou fora da caixa para sair"));
    }
}
