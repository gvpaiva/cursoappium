package Testes;

import Pages.MenuPage;
import Pages.SeuBarriga.*;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SBTeste extends BaseTest {
    MenuPage menu = new MenuPage();
    SBLoginPage login = new SBLoginPage();
    SBMenuPage menuSB = new SBMenuPage();
    SBContasPage contas = new SBContasPage();
    SBMovimentacoesPage mov = new SBMovimentacoesPage();
    SBHomePage home = new SBHomePage();
    SBResumoPage resumo = new SBResumoPage();
    @BeforeTest
    public void setUp() {
        menu.acessarSeuBarrigaNativo();
        login.setEmail("g@g");
        login.setSenha("123");
        login.entrar();

    }

    @Test (priority = 0)
    public void deveInserirContaComSucesso() {
        //entrar em conta
        menuSB.entrarContas();

        //inserir conta
        contas.setConta("Conta de teste");
        contas.salvar();

        //validar mensagem
        Assert.assertTrue(contas.existeElementoPorTexo("Conta adicionada com sucesso"));
    }

    @Test (priority = 1)
    public void deveExcluirConta() {
        //entrar em conta
        menuSB.entrarContas();

        //selecionar conta
        contas.selecionarConta("Conta de teste");

        //excluir
        contas.excluirConta();

        //validar mensagem
        Assert.assertTrue(contas.existeElementoPorTexo("Conta excluída com sucesso"));
    }
    @Test (priority = 2)
    public void deveValidarInclusaoMov(){
        menuSB.entrarMovimentacoes();

        // validar desc
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexo("Descrição é um campo obrigatório"));

        //validar interessado
        mov.setDescricao("Descrição Teste");
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexo("Interessado é um campo obrigatório"));

        //validar valor
        mov.setInteressado("Interessado Teste ");
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexo("Valor é um campo obrigatório"));
        //validar conta
        mov.setValor("10");
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexo("Conta é um campo obrigatório"));
        //inserir com sucesso
        mov.setConta("Conta para saldo");
        mov.salvar();
        Assert.assertTrue(mov.existeElementoPorTexo("Movimentação cadastrada com sucesso"));
    }
    @Test
    public void deveAtualizarSaldoAoExcluirMovimentacao(){
        //verificar saldo "Conta para Saldo" = 534.00
        Assert.assertEquals("534.00", home.obterSaldoConta("Conta para saldo"));
        //entrar em resumo
        menuSB.entraResumo();
        //excluir movimentacao 3
        resumo.excluirMovimentacao("Movimentacao 3, calculo saldo");
        //validar mensagem "Movimentacao removida com sucesso!"
        Assert.assertTrue(resumo.existeElementoPorTexo("Movimentação removida com sucesso!"));
        //Voltar para home
        menuSB.entrarHome();
        //atualizar o saldo
        Assert.assertTrue(menuSB.existeElementoPorTexo("Conta para saldo"));
        home.scroll(0.2,0.9);
        //verificar saldo = -1.000,00
        Assert.assertEquals("-1000.00", home.obterSaldoConta("Conta para saldo"));
    }
}
