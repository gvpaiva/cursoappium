package Testes;

import Pages.MenuPage;
import Pages.SwipeListPage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SwipeListTeste extends BaseTest {
    MenuPage menu = new MenuPage();
    SwipeListPage swipelist = new SwipeListPage();
    @Test
    public void deveRealizarDesafioSwipeList() {
        System.out.println(menu.existeSwipeList());
        //rolar até swipe
        menu.scrollDown();

        menu.acessarSwipelist();

        //swipe para direita
        swipelist.swipeElementEsquerda("Opção 1");
        //clicar mais
        swipelist.clicarBotao(285,90);
        //verificar op1+
        Assert.assertTrue(menu.existeElementoPorTexo("Opção 1 (+)"));
        //swipe para doreita
        swipelist.swipeElementEsquerda("Opção 4");
        //clicar menos
        swipelist.clicarBotao(392,432);
        //verificar op4-
        Assert.assertTrue(menu.existeElementoPorTexo("Opção 4 (-)"));

        //swipe para esquerda
        swipelist.swipeElementDireita("Opção 5 (-)");

        //verificar op5
        Assert.assertTrue(menu.existeElementoPorTexo("Opção 5"));
    }
}
