package Testes;

import Pages.DragNDropPage;
import Pages.MenuPage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DragAndDropTeste extends BaseTest {
    MenuPage menu = new MenuPage();
    DragNDropPage page = new DragNDropPage();

    String [] estadoInicial = new String[]{
            "Esta",
            "é uma lista",
            "Drag em Drop!",
            "Faça um clique longo,",
            "e arraste para",
            "qualquer local desejado.",};

    String [] estadoIntermediario = new String[]{
            "é uma lista",
            "Drag em Drop!",
            "Faça um clique longo,",
            "e arraste para",
            "Esta",
            "qualquer local desejado.",};
    String [] estadoFinal = new String[]{"Faça um clique longo,",
            "é uma lista",
            "Drag em Drop!",
            "e arraste para",
            "Esta",
            "qualquer local desejado.",
    };
    @Test
    public void deveEfetuarDragNDrop() throws InterruptedException {
        System.out.println(menu.existeSwipeList());
        //rolar até swipe
        menu.scrollDown();
        //acessar menu
        menu.acessarDragNDrop();

        //verificar estado inicial
        Assert.assertEquals(estadoInicial, page.obterLista());

        //arastar 'esta' para 'e arraste para'
        page.arrastar("Esta", "e arraste para");

        //verificar estado intermediario
        Assert.assertEquals(estadoIntermediario, page.obterLista());

        //arrastar "Faca um clique longo," para "é uma lista"
        page.arrastar("Faça um clique longo,", "é uma lista");

        page.obterLista();
        //verificar estado final
        Assert.assertEquals(estadoFinal, page.obterLista());
    }
}
