package Testes;
import Pages.MenuPage;
import Pages.SplashPage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SplashTeste extends BaseTest {
    private MenuPage menu = new MenuPage();
    private SplashPage splash = new SplashPage();

    @Test
    public void deveAguardarSplashSumir (){
        //Acessar splash
        menu.acessarSplash();
        //Verificar splash sendo exibido
        splash.isTelaSplashVisivel();
        //aguardar saida do splash
        splash.aguardarSplashSumir();
        //verificar que o formulario esta aparecendo
        Assert.assertTrue(splash.existeElementoPorTexo("Formulário"));
    }
}
