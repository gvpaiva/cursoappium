package Testes;

import Pages.FormularioPage;
import Pages.MenuPage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class FormularioTeste extends BaseTest {
    private MenuPage menu = new MenuPage();
    private FormularioPage formulario = new FormularioPage();
    @BeforeClass
    public void setUp (){
        menu.acessarFormulario();
    }

    @Test
    public void deveDigitarNome (){
        formulario.escreverNome("Gustavo Varela Paiva");
        Assert.assertEquals(formulario.obterNome(), "Gustavo Varela Paiva");
    }
    @Test
    public void deveInteragirComCombo(){
        formulario.selecionarCombo("PS4");

        String textoConsole = formulario.obterTexo();
        Assert.assertEquals(textoConsole, "PS4");
    }
    @Test
    public void deveInteragirSwitchECheckbox() {
        Assert.assertFalse(formulario.isCheckMarcado());
        Assert.assertTrue(formulario.isSwitchMarcado());

        formulario.clicarCheck();
        formulario.clicarSwitch();

        Assert.assertTrue(formulario.isCheckMarcado());
        Assert.assertFalse(formulario.isSwitchMarcado());
    }
    @Test
    public void salvarDemorado (){
        formulario.escreverNome("Gustavo Varela Paiva");
        Assert.assertEquals(formulario.obterNome(), "Gustavo Varela Paiva");

        formulario.clicarSalvarDemorado();
        formulario.esperarNomeFicarVisivel();

        String textoNomeCadastro = formulario.obterNomeCadastrado();
        Assert.assertEquals(textoNomeCadastro, "Nome: Gustavo Varela Paiva");
    }
    @Test
    public void deveAlterarData(){
        formulario.clicarPorTexto("01/01/2000");
        formulario.clicarPorTexto("20");
        formulario.clicarPorTexto("OK");

        Assert.assertTrue(formulario.existeElementoPorTexo("20/2/2000"));
    }
    @Test
    public void deveAlterarHora(){
        formulario.clicarPorTexto("06:00");
        formulario.clicarHora();
        formulario.clicarPorTexto("OK");

        Assert.assertTrue(formulario.existeElementoPorTexo("23:0"));
    }
    @Test
    public void deveInteragirComSeekbar() {
        //clicar no seekbar
        formulario.clicarSeek(0.88);
        //clicar no salvar
        formulario.clicarSalvar();
        //obter o valor
    }
}

