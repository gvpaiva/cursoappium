package Testes;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class CalculadoraTeste {
    @Test
    public void deveSomarDoisValores() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "Android");
        desiredCapabilities.setCapability("deviceName", "emulator-5554");
        desiredCapabilities.setCapability("automationName", "uiautomator2");
        desiredCapabilities.setCapability("appPackage", "com.android.calculator2");
        desiredCapabilities.setCapability("appActivity", "com.android.calculator2.Calculator");

        URL remoteUrl = new URL("http://localhost:4723/wd/hub");
        AndroidDriver navegador = new AndroidDriver(remoteUrl, desiredCapabilities);

        MobileElement el1 = (MobileElement) navegador.findElementById("com.android.calculator2:id/digit_2");
        el1.click();
        MobileElement el2 = (MobileElement) navegador.findElementByAccessibilityId("plus");
        el2.click();
        MobileElement el3 = (MobileElement) navegador.findElementById("com.android.calculator2:id/digit_2");
        el3.click();
        MobileElement el4 = (MobileElement) navegador.findElementById("com.android.calculator2:id/result");

        el4.click();

        System.out.println(el4.getText());
        Assert.assertEquals("4", el4.getText());
        navegador.quit();
    }
}

