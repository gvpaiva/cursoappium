package Testes;

import Pages.MenuPage;
import core.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SwipeTest extends BaseTest {
    MenuPage menu = new MenuPage();

    @Test
    public void deveRealizarSwipe() {
        System.out.println(menu.existeSwipe());

        //rolar até swipe
        menu.scroll(0.9, 0.2);

        //acessar menu
        menu.acessarSwipe();

        //verificar texto 'a esquerda'
        Assert.assertTrue(menu.existeElementoPorTexo("a esquerda"));

        //swipe para direita
        menu.swipeEsquerda();

        //verificar texto 'e veja se'
        Assert.assertTrue(menu.existeElementoPorTexo("E veja se"));

        //clicar botao direita
        menu.clicarPorTexto("›");

        //verificar texto 'Chegar até o fim!'
        Assert.assertTrue(menu.existeElementoPorTexo("Chegar até o fim!"));

        //swipe esquerda
        menu.swipeDireita();

        //clicar botao esquerda
        menu.clicarPorTexto("‹");
        //Verificar texto 'a esquerda'
        Assert.assertTrue(menu.existeElementoPorTexo("a esquerda"));
    }
}