package core;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

public class BaseTest {
    public String testName;

    @AfterMethod
    public void pegarNomedoTeste(Method method) {
        testName = method.getName();

    }
    @AfterTest
    public void fimTeste () {
        //DriverFactory.killDriver();
        gerarScreenshot();
    }
    public void gerarScreenshot() {
        File imagem = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(imagem, new File("target/screenshots/"+testName+".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void esperar(){

    }
}
