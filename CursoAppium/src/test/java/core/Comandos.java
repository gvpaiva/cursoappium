package core;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static core.DriverFactory.getDriver;
import static io.appium.java_client.touch.offset.ElementOption.element;

public class Comandos {
    public void escrever(By by, String texto) {
        getDriver().findElement(by).sendKeys(texto);
    }

    public String obterTexto(By by) {
        return getDriver().findElement(by).getText();
    }

    public void clicar(By by) {
        getDriver().findElement(by).click();
    }

    public void clicarPorTexto(String texto) {
        clicar(By.xpath("//*[@text= '" + texto + "']"));
    }

    public void tap(int x, int y) {
        TouchAction touchAction = new TouchAction(getDriver());
        touchAction.tap(PointOption.point(x, y)).perform();
    }

    public void selecionarConsole(By by, String valor) {
        getDriver().findElement(by).click();
        clicarPorTexto(valor);
    }

    public boolean isCheckMarcado(By by) {
        return getDriver().findElement(by).getAttribute("checked").equals("true");
    }

    public void esperarFicarVisivel(By by) {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public boolean existeElementoPorTexo(String texto) {
        List<MobileElement> elementos = getDriver().findElements(By.xpath("//*[@text= '" + texto + "']"));
        return elementos.size() > 0;
    }

    public String obterMensagemAlerta() {
        return obterTexto(By.id("android:id/message"));
    }

    public void naoExisteElemento(By by) {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }
    public void scrollDown(){
        scroll(0.9, 0.1);
    }

    public void scrollUp(){
        scroll(0.1, 0.9);
    }
    public void scroll(double inicio, double fim) {
        Dimension tamanho = getDriver().manage().window().getSize();

        int x = tamanho.width / 2;
        int yInicial = (int) (tamanho.height * inicio);
        int yFinal = (int) (tamanho.height * fim);
        new TouchAction(getDriver())
                .press(PointOption.point(x, yInicial))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
                .moveTo(PointOption.point(x, yFinal))
                .release()
                .perform();
    }
    public void swipeEsquerda(){
        swipe(0.1,0.9);
    }
    public void swipeDireita(){
        swipe(0.9,0.1);
    }
    public void swipe(double inicio, double fim) {
        Dimension tamanho = getDriver().manage().window().getSize();

        int y = tamanho.height / 2;

        int xInicial = (int) (tamanho.width * inicio);
        int xFinal = (int) (tamanho.width * fim);
        new TouchAction(getDriver())
                .press(PointOption.point(xInicial, y))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(10)))
                .moveTo(PointOption.point( xFinal, y))
                .release()
                .perform();
    }
    public void swipeElement(MobileElement elemento, double inicio, double fim) {
        int y = elemento.getLocation().y + (elemento.getSize().height /2);

        int xInicial = (int) (elemento.getSize().width * inicio);
        int xFinal = (int) (elemento.getSize().width * fim);
        new TouchAction(getDriver())
                .press(PointOption.point(xInicial, y))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(10)))
                .moveTo(PointOption.point( xFinal, y))
                .release()
                .perform();
    }
    public void cliqueLongo (By by){
        WebElement element = getDriver().findElement(by);
        TouchAction action = new TouchAction(getDriver()).
                longPress(LongPressOptions.longPressOptions().
                        withElement(element(element)).
                        withDuration(Duration.ofMillis(2000))).
                release().
                perform();
    }

}
