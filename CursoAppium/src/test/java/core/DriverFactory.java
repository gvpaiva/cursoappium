package core;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

public class DriverFactory {
    private static AndroidDriver navegador = null;

    public static AndroidDriver getDriver () {
        if (navegador == null) {
            createDriver();
        }
        return navegador;
    }
    private static void createDriver () {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "Android");
        desiredCapabilities.setCapability("deviceName", "Y5VSPRHMF6WSU8GA");
        desiredCapabilities.setCapability("automationName", "uiautomator2");
        desiredCapabilities.setCapability(MobileCapabilityType.APP, "C:\\Users\\gvpaiva\\IdeaProjects\\CursoAppium\\src\\main\\resources\\CTAppium-1-1.apk");
        AppiumServiceBuilder builder = new AppiumServiceBuilder();
        builder.withIPAddress("127.0.0.1");
        builder.usingPort(4724);
        builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
        builder.withCapabilities(desiredCapabilities);

        AppiumDriverLocalService service = AppiumDriverLocalService.buildService(builder);
        navegador = new AndroidDriver(service, desiredCapabilities);
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    public static void killDriver() {
        if (navegador != null){
            navegador.quit();
            navegador = null;
        }
    }
}
