package Pages.SeuBarriga;

import core.Comandos;
import org.openqa.selenium.By;

public class SBContasPage extends Comandos {
    public void setConta(String conta) {
        escrever(By.className("android.widget.EditText"), conta);
    }
    public void salvar(){
        clicarPorTexto("SALVAR");
    }
    public void selecionarConta(String conta){
        cliqueLongo(By.xpath("//*[@text='"+conta+"']"));
    }
    public void excluirConta(){
        clicarPorTexto("EXCLUIR");
    }
}
