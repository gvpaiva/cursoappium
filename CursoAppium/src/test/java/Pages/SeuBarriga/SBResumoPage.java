package Pages.SeuBarriga;

import core.Comandos;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

import static core.DriverFactory.getDriver;

public class SBResumoPage extends Comandos {
    public void excluirMovimentacao (String desc) {
       MobileElement el = (MobileElement) getDriver().findElement(By.xpath("//*[@text='"+desc+"']/.."));
        swipeElement(el,0.9, 0.1);
        tap(439, 685);
    }
}
