package Pages.SeuBarriga;

import core.Comandos;
import org.openqa.selenium.By;

public class SBMovimentacoesPage extends Comandos {
    public void salvar(){
        clicarPorTexto("SALVAR");
    }
    public void setDescricao(String desc) {
        escrever(By.className("android.widget.EditText"),desc );
    }
    public void setInteressado(String interessado) {
        escrever(By.xpath("//android.widget.EditText[2]"), interessado);
    }
    public void setValor(String valor) {
        escrever(By.xpath("//android.widget.EditText[3]"), valor);
    }
    public void setConta(String conta){
        selecionarConsole(By.xpath("//android.widget.Spinner[2]"), conta);
    }
}
