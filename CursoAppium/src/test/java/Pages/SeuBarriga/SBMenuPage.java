package Pages.SeuBarriga;

import core.Comandos;

public class SBMenuPage extends Comandos {
    public void entrarContas() {
        clicarPorTexto("CONTAS");
    }
    public void entrarMovimentacoes(){
        clicarPorTexto("MOV...");
    }
    public void entraResumo(){
        clicarPorTexto("RESUMO");
    }
    public void entrarHome(){
        clicarPorTexto("HOME");
    }
}
