package Pages;

import core.Comandos;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.touch.TouchActions;

import static core.DriverFactory.getDriver;

public class SwipeListPage extends Comandos {

    public void swipeElementDireita(String opcao) {
        swipeElement((MobileElement) getDriver().findElement(By.xpath("//*[@text= '" + opcao + "']/..")),0.1, 0.9);
    }

    public void swipeElementEsquerda(String opcao) {
        swipeElement((MobileElement) getDriver().findElement(By.xpath("//*[@text= '" + opcao + "']/..")),0.9, 0.1);
    }
    public void  clicarBotao(int x, int y) {
        new TouchAction(getDriver())
                .tap(PointOption.point(x,y))
                .perform();
    }

}
