package Pages;

import core.Comandos;
import core.DriverFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static core.DriverFactory.getDriver;

public class DragNDropPage extends Comandos {
    public void arrastar(String origem, String destino) {
        WebElement inicio = getDriver().findElement(By.xpath("//*[@text='" + origem + "']"));
        WebElement fim = getDriver().findElement(By.xpath("//*[@text='" + destino + "']"));
        ;
        new TouchAction(getDriver())
                .longPress(ElementOption.element(inicio))
                .moveTo(ElementOption.element(fim))
                .release()
                .perform();
    }

    public String[] obterLista() {
        WebDriverWait wait = new WebDriverWait(getDriver(),10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.TextView")));
        List<MobileElement> elements = getDriver().findElements(By.className("android.widget.TextView"));
        String[] retorno = new String[elements.size()];
        for (int i = 0; i < elements.size(); i++){
            retorno[i] = elements.get(i).getText();

        }
        return retorno;
    }
}
