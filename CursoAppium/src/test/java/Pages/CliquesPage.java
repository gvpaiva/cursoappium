package Pages;
import core.Comandos;
import org.openqa.selenium.By;
import static core.DriverFactory.getDriver;

public class CliquesPage extends Comandos {
    public void cliqueLongo() {
        cliqueLongo(By.xpath("//*[@text='Clique Longo']"));
    }
    public String obterTextoCampo() {
        return getDriver().findElement(By.xpath("(//android.widget.TextView)[3]")).getText();
    }
}
