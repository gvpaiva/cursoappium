package Pages;

import core.Comandos;
import org.openqa.selenium.By;

public class AlertaPage extends Comandos {
    public void clicarAlertaConfirm(){
        clicarPorTexto(("ALERTA CONFIRM"));
    }
    public String obterTituloAlerta (){
        return obterTexto(By.id("android:id/alertTitle"));
    }
    public String obterMensagemAlerta (){
        return obterTexto(By.id("android:id/message"));
    }
    public void clicarConfirmar(){
        clicar(By.id("android:id/button2"));
    }
    public void esperarMensagemFicarVisivel(){
        esperarFicarVisivel(By.id("android:id/message"));
    }
    public void clicarSair(){
        clicar(By.id("android:id/button1"));
    }
    public void clicarAlertaSimples(){
        clicarPorTexto(("ALERTA SIMPLES"));
    }
    public void esperarConfirmarFicarVisivel(){
        esperarFicarVisivel(By.id("android:id/button1"));
    }
    public void clicarForaCaixa (){
        tap(160, 60);
    }
}
