package Pages;

import core.Comandos;
import org.openqa.selenium.By;

public class MenuPage extends Comandos {
    public void acessarFormulario() {
        clicarPorTexto("Formulário");
    }

    public void acessarSplash() {
        clicarPorTexto("Splash");
    }

    public void acessarAlerta() {
        clicarPorTexto("Alertas");
    }

    public void acessarAbas() {
        clicarPorTexto("Abas");
    }

    public void acessarAccordion() {
        clicarPorTexto("Accordion");
    }

    public void acessarCliques() {
        clicarPorTexto("Cliques");
    }

    public boolean existeOpcao() {
        return existeElementoPorTexo("Opção bem escondida");
    }
    public boolean existeSwipe() {
        return existeElementoPorTexo("Swipe");
    }
    public boolean existeSwipeList() {
        return existeElementoPorTexo("Swipe List");
    }
    public boolean existeDragNDrop() {
        return existeElementoPorTexo("Drag and drop");
    }

    public void acessarSwipe() {
        clicarPorTexto("Swipe");
    }
    public void acessarSwipelist() {
        clicarPorTexto("Swipe List");
    }

    public void acessarDragNDrop() {
        clicarPorTexto("Drag and drop");
    }

    public void acessarSeuBarrigaHibrido() {
        clicarPorTexto("SeuBarriga Híbrido");
    }
    public void acessarSeuBarrigaNativo() {
        clicarPorTexto("SeuBarriga Nativo");
    }
}
