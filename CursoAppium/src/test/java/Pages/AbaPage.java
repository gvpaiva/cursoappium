package Pages;

import core.Comandos;

public class AbaPage extends Comandos {
    public boolean obterTextoAba1 (){
        return existeElementoPorTexo("Este é o conteúdo da Aba 1");
    }
    public boolean obterTextoAba2 (){
        return existeElementoPorTexo("Este é o conteúdo da Aba 2");
    }
    public void selecionarAba2 (){
        clicarPorTexto("ABA 2");
    }
}
