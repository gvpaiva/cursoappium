package Pages;

import core.Comandos;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import static core.DriverFactory.getDriver;

public class FormularioPage extends Comandos {
    public void escreverNome(String nome) {
        escrever(MobileBy.AccessibilityId("nome"), nome);
    }

    public String obterNome() {
        return obterTexto(MobileBy.AccessibilityId("nome"));
    }

    public void selecionarCombo(String valor) {
        selecionarConsole(MobileBy.AccessibilityId("console"), valor);
    }

    public String obterTexo() {
        return obterTexto(By.xpath("//android.widget.Spinner/android.widget.TextView"));
    }

    public void clicarCheck() {
        clicar(By.className("android.widget.CheckBox"));
    }

    public void clicarSwitch() {
        clicar(MobileBy.AccessibilityId("switch"));
    }

    public boolean isCheckMarcado() {
        return (isCheckMarcado(By.className("android.widget.CheckBox")));
    }

    public boolean isSwitchMarcado() {
        return (isCheckMarcado(MobileBy.AccessibilityId("switch")));
    }

    public void clicarSalvar() {
        clicar(MobileBy.AccessibilityId("save"));
    }

    public void clicarSalvarDemorado() {
        clicarPorTexto("SALVAR DEMORADO");
    }

    public String obterNomeCadastrado() {
        return obterTexto(By.xpath("//android.widget.TextView[@text='Nome: Gustavo Varela Paiva']"));
    }

    public String obterConsoleCadastrado() {
        return obterTexto(By.xpath("//android.widget.TextView[starts-with(@text, 'Console:')]"));
    }

    public void esperarNomeFicarVisivel() {
        esperarFicarVisivel(By.xpath("//android.widget.TextView[@text='Nome: Gustavo Varela Paiva']"));
    }

    public void clicarHora() {
        clicar(MobileBy.AccessibilityId("23"));
    }

    public void clicarSeek(double posicao) {
        int delta = 20;
        MobileElement seek = (MobileElement) getDriver().findElement(MobileBy.AccessibilityId("slid"));
        int y = seek.getLocation().y + (seek.getSize().height/2);
        System.out.println(y);

        int x1 = seek.getLocation().x + delta;
        int x = (int) (x1 + ((seek.getSize().width - 2*delta) * posicao));
        System.out.println(x);
        tap(x,y);
    }
}
