package Pages;

import core.Comandos;
import org.openqa.selenium.By;

public class SplashPage extends Comandos {
    public boolean isTelaSplashVisivel (){
        return existeElementoPorTexo("Splash!");
    }
    public void aguardarSplashSumir(){
        naoExisteElemento(By.xpath("//*[@text= 'Splash!']"));
    }
}
